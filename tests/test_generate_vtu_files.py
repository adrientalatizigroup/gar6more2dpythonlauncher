import pytest
import os
from gar6more2dpythonlauncher.src.generate_vtu_files import Quadrangle, Triangle, GridNode, StructuredGridQuad, StructuredGridTri

@pytest.fixture
def sample_quadrangle():
    return Quadrangle(1, 2, 3, 4)

@pytest.fixture
def sample_triangle():
    return Triangle(1, 2, 3)

@pytest.fixture
def sample_grid_node():
    return GridNode(0.5, 0.5)

@pytest.fixture
def sample_structured_grid_quad():
    return StructuredGridQuad(1.0, 2)

@pytest.fixture
def sample_structured_grid_tri():
    return StructuredGridTri(1.0, 2)

def test_quadrangle_initialization(sample_quadrangle):
    assert sample_quadrangle.N1 == 1
    assert sample_quadrangle.N2 == 2
    assert sample_quadrangle.N3 == 3
    assert sample_quadrangle.N4 == 4
    assert sample_quadrangle.node_tags == [1, 2, 3, 4]

def test_triangle_initialization(sample_triangle):
    assert sample_triangle.N1 == 1
    assert sample_triangle.N2 == 2
    assert sample_triangle.N3 == 3
    assert sample_triangle.node_tags == [1, 2, 3]

def test_grid_node_initialization(sample_grid_node):
    assert sample_grid_node.x == 0.5
    assert sample_grid_node.y == 0.5

def test_structured_grid_quad_initialization(sample_structured_grid_quad):
    assert len(sample_structured_grid_quad.nodes) == 9  # (2+1) x (2+1) = 9 nodes for a 2x2 grid
    assert len(sample_structured_grid_quad.quadrangles) == 4  # 2x2 grid forms 4 quadrangles

def test_structured_grid_tri_initialization(sample_structured_grid_tri):
    assert len(sample_structured_grid_tri.nodes) == 9  # (2+1) x (2+1) = 9 nodes for a 2x2 grid
    assert len(sample_structured_grid_tri.triangles) == 8  # 2x2 grid forms 8 triangles


def test_add_field(sample_structured_grid_quad):
    # Given
    sample_structured_grid_quad.add_field("Field1", [1, 2, 3, 4])

    # Then
    assert "Field1" in sample_structured_grid_quad.MeshioMesh.point_data
    assert (sample_structured_grid_quad.MeshioMesh.point_data["Field1"] == [1, 2, 3, 4]).all()

def test_dump(sample_structured_grid_quad):
    # Given
    file_name = "test_dump_quad"

    # When
    sample_structured_grid_quad.dump(file_name)

    # Then
    assert os.path.exists(f"{file_name}.vtu")

    # Clean up
    os.remove(f"{file_name}.vtu")


def test_add_field_tri(sample_structured_grid_tri):
    # Given
    sample_structured_grid_tri.add_field("Field1", [1, 2, 3, 4, 5, 6, 7, 8, 9])

    # Then
    assert "Field1" in sample_structured_grid_tri.MeshioMesh.point_data
    assert (sample_structured_grid_tri.MeshioMesh.point_data["Field1"] == [1, 2, 3, 4, 5, 6, 7, 8, 9]).all()

def test_dump_tri(sample_structured_grid_tri):
    # Given
    file_name = "test_dump_tri"

    # When
    sample_structured_grid_tri.dump(file_name)

    # Then
    assert os.path.exists(f"{file_name}.vtu")

    # Clean up
    os.remove(f"{file_name}.vtu")


def test_build_setup_grid(sample_structured_grid_quad):
    # Given
    length_vec_length = sample_structured_grid_quad.nb_of_nodes_per_line

    # When
    sample_structured_grid_quad._StructuredGridQuad__build_setup_grid()

    # Then
    assert len(sample_structured_grid_quad.setup_grid) == length_vec_length

    for line in sample_structured_grid_quad.setup_grid:
        assert len(line) == length_vec_length

def test_build_quadrangle_tags(sample_structured_grid_quad):
    # Given
    P0_idx, P1_idx, P2_idx, P3_idx = 0, 1, 2, 3

    # When
    Q = sample_structured_grid_quad._StructuredGridQuad__build_quadrangle_tags(P0_idx, P1_idx, P2_idx, P3_idx)

    # Then
    assert isinstance(Q, Quadrangle)
    assert Q.N1 == P0_idx
    assert Q.N2 == P1_idx
    assert Q.N3 == P2_idx
    assert Q.N4 == P3_idx

def test_build_node_indexes_from_grid_position(sample_structured_grid_quad):
    # Given
    i = 1
    j = 1
    expected_result = (4, 5, 8, 7)  # Corrected expected node indexes for the given grid position

    # When
    result = sample_structured_grid_quad._StructuredGridQuad__build_node_indexes_from_grid_position(i, j)

    # Then
    assert result == expected_result


def test_build_nodes(sample_structured_grid_quad):
    # Given
    expected_num_nodes = 9  # Expected number of nodes in the grid (3x3)
    
    # When
    sample_structured_grid_quad._StructuredGridQuad__build_nodes()
    
    # Then
    assert len(sample_structured_grid_quad.nodes) == expected_num_nodes

