"""
Unit Tests for get_lines_of_file Function

This file contains pytest unit tests for the get_lines_of_file function from the
gar6more2dpythonlauncher.src.txt_file_gestion_fcts module. The tests cover scenarios for both existing,
non-existent, and empty files, ensuring the function behaves correctly in different cases.

Test Descriptions:
- test_get_lines_of_file_existing_file:
  Checks that get_lines_of_file returns the expected lines for an existing file.

- test_get_lines_of_file_nonexistent_file:
  Checks that get_lines_of_file raises a FileNotFoundError for a non-existent file.

- test_get_lines_of_file_empty_file:
  Checks that get_lines_of_file returns an empty list for an empty file.

Note: The tests use the pytest.raises context manager to verify the expected exceptions.

Date: 09/11/2023
"""

import pytest

# Assuming your function and check_file_exists are defined in a module named 'your_module'
from gar6more2dpythonlauncher.src.txt_file_gestion_fcts import  get_lines_of_file

def test_get_lines_of_file_existing_file(tmp_path):
    # Create a temporary file with known content
    test_file = tmp_path / "test_file.txt"
    test_content = "Line 1\nLine 2\nLine 3"
    test_file.write_text(test_content)

    # Get the lines using the function
    lines = get_lines_of_file(test_file)

    # Check if the returned lines match the expected content
    assert lines == ["Line 1\n", "Line 2\n", "Line 3"]

def test_get_lines_of_file_nonexistent_file():
    # Test that the function raises a FileNotFoundError for a non-existent file
    with pytest.raises(FileNotFoundError):
        get_lines_of_file("nonexistent_file.txt")

def test_get_lines_of_file_empty_file(tmp_path):
    # Create a temporary empty file
    test_file = tmp_path / "empty_file.txt"
    test_file.touch()

    # Get the lines using the function
    lines = get_lines_of_file(test_file)

    # Check if the returned lines are an empty list
    assert lines == []
