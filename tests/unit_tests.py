"""
Unit Tests for `update_gar6_str_with_new_reicv_height` Function

This file contains unit tests using pytest for the update_gar6_str_with_new_reicv_height function.
The function is responsible for updating the height of the line of receivers in the Gar6more2D.dat file.

Tested Scenarios:
1. Valid update with positive integer height: 36
2. Valid update with negative integer height: -21
3. Valid update with small float height: 0.001
4. Valid update with float height: 0.1
5. Valid update with another float height: 0.56
6. Handling non-existent file (raises FileNotFoundError)
7. Handling invalid height value (raises ValueError)

Note: The temporary files are created and cleaned up during the tests.

Author: 
Date: 09/11/2023
"""

import pytest
import tempfile
import os

# Assuming your function and get_Gar6_dat are defined in a module named 'your_module'
from launch import update_gar6_str_with_new_reicv_height, get_Gar6_dat


@pytest.mark.parametrize("new_height", [-21, 36, 0.001, 0.1, 0.56])
def test_update_gar6_str_with_new_reicv_height(new_height):
    # Create a temporary file with sample content
    with tempfile.NamedTemporaryFile(mode="w+", delete=False) as temp_file:
        temp_file.write("Initial content\n")
        temp_file.write("Height of the line of receivers\n")
        temp_file.write("Some more content\n")

    # Update the height using the function
    update_gar6_str_with_new_reicv_height(temp_file.name, new_height=new_height)

    # Read the updated content
    with open(temp_file.name, "r") as f:
        updated_content = f.read()

    # Check if the height is updated
    assert f"{float(new_height)} Height of the line of receivers" in updated_content

    # Cleanup: Delete the temporary file
    os.remove(temp_file.name)



def test_update_gar6_str_with_new_reicv_height_nonexistent_file():
    # Ensure the function handles non-existent files gracefully
    with pytest.raises(FileNotFoundError):
        update_gar6_str_with_new_reicv_height("nonexistent_file.dat", new_height=42)


def test_update_gar6_str_with_new_reicv_height_invalid_height():
    # Ensure the function raises a ValueError for invalid height values
    with tempfile.NamedTemporaryFile(mode="w+", delete=False) as temp_file:
        temp_file.write("Initial content\n")
        temp_file.write("Height of the line of receivers\n")
        temp_file.write("Some more content\n")

    # Attempt to update with an invalid height (string instead of float)
    with pytest.raises(ValueError):
        update_gar6_str_with_new_reicv_height(temp_file.name, new_height="invalid_height")

    # Cleanup: Delete the temporary file
    os.remove(temp_file.name)
