import os
import shutil

from .check_current_directory import check_current_directory


def rename_file(actual_filename, new_filename):
    """
    Rename a file.

    Parameters:
    - actual_filename (str): The current name of the file.
    - new_filename (str): The new name for the file.

    Returns:
    - bool: True if the file is successfully renamed, False otherwise.
    """
    try:
        # Check if the file exists before attempting to rename
        if os.path.exists(actual_filename):
            # Use the os.rename() method to rename the file
            os.rename(actual_filename, new_filename)
            print(f"File '{actual_filename}' successfully renamed to '{new_filename}'.")
            return True
        else:
            print(f"Error: File '{actual_filename}' not found.")
            return False
    except Exception as e:
        print(f"Error: {e}")
        return False


def transfer_a_file_into_a_dir(filename, dirname):
    """
    Transfer (move) a file into a directory.

    Parameters:
    - filename (str): The name of the file to be moved.
    - dirname (str): The name of the destination directory.

    Returns:
    - bool: True if the file is successfully moved, False otherwise.
    """
    try:
        # Check if the file exists before attempting to move
        if os.path.exists(filename):
            # Check if the destination directory exists, create if not
            if not os.path.exists(dirname):
                os.makedirs(dirname)
            
            # Get the base name of the file without the extension
            base_filename, file_extension = os.path.splitext(os.path.basename(filename))
            
            suffix = 0

            # Construct the full destination path
            dest_path = os.path.join(dirname, f"{base_filename}_{suffix}.txt")

            # If the file already exists, append a suffix until a unique name is found
            while os.path.exists(dest_path):
                suffix += 1
                dest_path = os.path.join(dirname, f"{base_filename}_{suffix}.txt")

            # Use shutil.move() to move the file into the directory
            shutil.move(filename, dest_path)
            print(f"File '{filename}' successfully moved to directory '{dirname}' as '{os.path.basename(dest_path)}'.")
            return True
        else:
            print(f"Error: File '{filename}' not found.")
            return False
    except Exception as e:
        print(f"Error: {e}")
        return False



def transfer_result_files_into_result_directory():
    """
    Transfer result files 'Ux.dat' and 'Uy.dat' into the 'Results' directory.

    This function first checks if the current working directory is 'gar6more'.
    If not, it raises a RuntimeError. Then, it uses the transfer_a_file_into_a_dir
    function to move 'Ux.dat' and 'Uy.dat' into the 'Results' directory.

    Note: The 'transfer_a_file_into_a_dir' function should be defined and imported
    in the same module or available in the current Python environment.

    Raises:
    - RuntimeError: If the current working directory is not 'gar6more'.
    """
    # Check if the current working directory is 'gar6more'
    check_current_directory('gar6more2d')

    # Move 'Ux.dat' into 'gar6more2dpythonlauncher/Results'
    transfer_a_file_into_a_dir("Ux.dat", "gar6more2dpythonlauncher/Results")

    # Move 'Uy.dat' into 'gar6more2dpythonlauncher/Results'
    transfer_a_file_into_a_dir("Uy.dat", "gar6more2dpythonlauncher/Results")
