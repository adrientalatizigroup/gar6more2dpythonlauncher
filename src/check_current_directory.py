import os

def check_current_directory(expected_directory):
    """
    Check if the current working directory is the expected directory.
    If not, raise an error.

    Parameters:
    - expected_directory (str): The expected directory name.

    Raises:
    - RuntimeError: If the current working directory is not the expected directory.
    """
    current_directory = os.getcwd()
    if not os.path.basename(current_directory) == expected_directory:
        raise RuntimeError(f"Error: Expected to be in directory '{expected_directory}', but current directory is '{current_directory}'.")
    else:
        print(f"In the expected directory: '{current_directory}'.")
