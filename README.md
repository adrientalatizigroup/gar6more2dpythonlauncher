# Gar6more Python Launcher

The Gar6more Python Launcher is a utility designed to facilitate the execution of the Gar6more Fortran90 program for computing analytical solutions of wave equations on a line. This Python program orchestrates the execution of the Gar6more executable multiple times to gather information on multiple lines, forming a grid. It then outputs the results as .vtu files, which can be visualized in various visualization tools supporting the VTK format.
## Features

Automates the execution of the Gar6more Fortran90 program.
Collects data for multiple lines to form a grid.
Generates .vtu files containing the computed wave equation solutions.
Supports command-line arguments to specify the range of lines and the number of segments.

## Prerequisites

- Gar6more Fortran90 executable.
- Python 3.x installed.
- Meshio library for Python (install via pip install meshio).

## Usage

- Place the gar6more2dpythonlauncher directory inside the Gar6more2D directory.
- Navigate to the Gar6more directory in the terminal.
- Execute the following command to launch the Python Launcher:

```bash
    python3 gar6more2dpythonlauncher/launcher.py --range -1.0 1.0 201
```
This command will compute the analytical solutions for lines ranging from -1.0 to 1.0 with 201 segments in height.

## Notes

1. Ensure that the command line is executed from the Gar6more directory.
    
2. The .vtu files containing the computed solutions will be written directly inside the Gar6more directory.

3. Very important: StructuredGrid have been create to manage only square grids. Be sure that there is a same number of receivers per line than the number of heights segments, as well as the starting/ending abscissa of the line of receivers if the same than the height range when using python launcher.