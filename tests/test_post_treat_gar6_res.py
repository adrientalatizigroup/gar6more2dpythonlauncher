"""
Unit tests for functions related to processing DAT files.

These tests verify the behavior of functions for reading and 
combining DAT files, as well as calculating L2 norms.
"""

import numpy as np
import pytest
from gar6more2dpythonlauncher.src.post_treat_gar6_res import read_dat_file, combine_dat_files



# Fixture to create a temporary DAT file for testing
@pytest.fixture
def create_temp_dat_file(tmpdir):
    file_content = """0.0000000000000000 -0.1 -0.2 -0.3
1.000000000000000E-005 -0.4 -0.5 -0.6
2.000000000000000E-005 -0.7 -0.8 -0.9
3.000000000000000E-005 -1.0 -1.1 -1.2
"""
    file_path = tmpdir.join("temp_file.dat")
    file_path.write(file_content)
    return str(file_path)

# Fixture to create an empty temporary DAT file for testing
@pytest.fixture
def create_empty_dat_file(tmpdir):
    file_path = tmpdir.join("empty_temp_file.dat")
    file_path.write("")  # Empty content
    return str(file_path)

# Fixture to create temporary DAT files for testing
@pytest.fixture
def create_temp_dat_files(tmpdir):
    file1_content = """0.0000000000000000 -0.1 -0.2 -0.3
1.000000000000000E-005 -0.4 -0.5 -0.6
2.000000000000000E-005 -0.7 -0.8 -0.9
3.000000000000000E-005 -1.0 -1.1 -1.2
"""
    file2_content = """0.0000000000000000 1.0 2.0 3.0
1.000000000000000E-005 4.0 5.0 6.0
2.000000000000000E-005 7.0 8.0 9.0
3.000000000000000E-005 10.0 11.0 12.0
"""

    file1_path = tmpdir.join("temp_file1.dat")
    file2_path = tmpdir.join("temp_file2.dat")

    file1_path.write(file1_content)
    file2_path.write(file2_content)

    return str(file1_path), str(file2_path)


# Fixture to create a sample dictionary for testing
@pytest.fixture
def sample_input_dict():
    return {
        0.0: [(-0.1, 1.0), (-0.2, 2.0), (-0.3, 3.0)],
        1e-05: [(-0.4, 4.0), (-0.5, 5.0), (-0.6, 6.0)],
        2e-05: [(-0.7, 7.0), (-0.8, 8.0), (-0.9, 9.0)],
        3e-05: [(-1.0, 10.0), (-1.1, 11.0), (-1.2, 12.0)]
    }






def test_read_dat_file(create_temp_dat_file, create_empty_dat_file):
    # Given
    file_path = create_temp_dat_file

    # When
    result_dict = read_dat_file(file_path)

    print(result_dict)

    # Then
    assert len(result_dict) == 4  # Check the number of entries in the dictionary

    # Check the values for specific keys
    assert np.array_equal(result_dict[0.0], np.array([-0.1, -0.2, -0.3]))
    assert np.array_equal(result_dict[1.0e-5], np.array([-0.4, -0.5, -0.6]))
    assert np.array_equal(result_dict[2.0e-5], np.array([-0.7, -0.8, -0.9]))
    assert np.array_equal(result_dict[3.0e-5], np.array([-1.0, -1.1, -1.2]))

    # Check for a non-existing key
    assert 1.0 not in result_dict

    # Check for an empty file
    empty_file_path = create_empty_dat_file  # Reuse the fixture with an empty file
    empty_dict = read_dat_file(empty_file_path)

    print("empty_dict: ", empty_dict)

    assert len(empty_dict) == 0

    # Check for a non-existing file
    non_existing_file = "non_existing_file.dat"
    with pytest.raises(FileNotFoundError):
        read_dat_file(non_existing_file)

    # Check for invalid content in the file
    invalid_content_file = create_temp_dat_file
    with open(invalid_content_file, 'a') as file:
        file.write("Invalid Line\n")
    with pytest.raises(ValueError):
        read_dat_file(invalid_content_file)


def test_combine_dat_files(create_temp_dat_files):
    # Given
    file1_path, file2_path = create_temp_dat_files

    # When
    result_dict = combine_dat_files(file1_path, file2_path)

    print("result dict: ", result_dict)

    # Then
    assert len(result_dict) == 4  # There are four keys in both files
    
    
    expected_dict =  {
        0.0: [(-0.1, 1.0), (-0.2, 2.0), (-0.3, 3.0)],
        1e-05: [(-0.4, 4.0), (-0.5, 5.0), (-0.6, 6.0)],
        2e-05: [(-0.7, 7.0), (-0.8, 8.0), (-0.9, 9.0)],
        3e-05: [(-1.0, 10.0), (-1.1, 11.0), (-1.2, 12.0)]
    }

    for key, val in result_dict.items():
        assert result_dict[key] == expected_dict[key]