import numpy as np

def read_dat_file(file_path):
    """
    Read data from a .dat file and store it in a dictionary.

    Parameters:
    - file_path (str): The path to the .dat file.

    Returns:
    - dict: A dictionary where the key is the data of the first column, 
            and the value is a NumPy array of the current line (excluding the first column data).
    """
    data_dict = {}

    with open(file_path, 'r') as file:
        for line in file:
            # Split the line into values
            values = line.strip().split()

            # Convert the first column data to a float (use as key)
            key = float(values[0])

            # Convert the remaining values to a NumPy array (excluding the first column)
            data_array = np.array([float(value) for value in values[1:]])

            # Store the data in the dictionary
            data_dict[key] = data_array

    return data_dict

def combine_dat_files(file1, file2):
    """
    Combine data from two DAT files into a single dictionary.

    Parameters:
    - file1 (str): Path to the first DAT file.
    - file2 (str): Path to the second DAT file.

    Returns:
    - dict: Combined dictionary where keys are from file1 and values are pairs of corresponding values from both files.
    """
    # Read data from the first file
    d1 = read_dat_file(file1)

    # Read data from the second file
    d2 = read_dat_file(file2)

    # Combined dictionary to store the result
    combined_dict = {}

    # Iterate over keys in the first dictionary
    for key in d1:
        # Ensure the key is present in the second dictionary
        if key in d2:
            # Combine corresponding values from both dictionaries
            combined_values = list(zip(d1[key], d2[key]))

            # Update the combined dictionary
            combined_dict[key] = combined_values

    return combined_dict


def get_Ux_and_Uy_to_dict(n):
    
    """
    Generate a dictionary containing Ux and Uy data for a range of iterations.

    Parameters:
    - n (int): The number of iterations.

    Returns:
    - dict: A dictionary where keys are iterations, and values are lists of L2 norms for Ux and Uy.
    """
    # Prefix for file paths
    prefix = "gar6more2dpythonlauncher/Results/U"

    # Initialize the dictionary with Ux data for iteration 0
    d_grid = read_dat_file(f"{prefix}x_0.txt")

    # Initialize lists for each key in the dictionary
    for key in d_grid:
        d_grid[key] = list()

    for k in range(int(n)):
        # Calculate L2 norms for combined Ux and Uy data
        dn = combine_dat_files(f"{prefix}x_{k}.txt", f"{prefix}y_{k}.txt")
        # Append L2 norms to the corresponding lists in the dictionary
        for key, val in dn.items():
            d_grid[key].append(val)

    d_grid_ordered= dict()
    count = 0
    for key,val in d_grid.items():
        val.reverse()
        d_grid_ordered[count]= np.array(val)
        count += 1
    d_grid= d_grid_ordered

    return d_grid
