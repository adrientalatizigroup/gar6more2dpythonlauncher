import os
import shutil
import pytest
from gar6more2dpythonlauncher.src.gather_results import rename_file, transfer_a_file_into_a_dir

# Test cases for rename_file function
def test_rename_file_successful():
    # Create a temporary file
    with open('temp_file.txt', 'w') as file:
        file.write('Hello, World!')
    
    assert rename_file('temp_file.txt', 'new_temp_file.txt') is True
    assert os.path.exists('new_temp_file.txt') is True
    assert os.path.exists('temp_file.txt') is False

def test_rename_file_file_not_found():
    assert rename_file('nonexistent_file.txt', 'new_file.txt') is False

# Test cases for transfer_a_file_into_a_dir function
def test_transfer_a_file_into_a_dir_successful():
    # Create a temporary file
    with open('temp_file.txt', 'w') as file:
        file.write('Hello, World!')

    # Perform the file transfer
    assert transfer_a_file_into_a_dir('temp_file.txt', 'temp_directory') is True

    # Check if the renamed file exists in the destination directory
    assert os.path.exists('temp_directory/temp_file_0.txt') is True

    # Check if the original file does not exist in the source directory
    assert os.path.exists('temp_file.txt') is False

@pytest.fixture
def cleanup_temp_files(request):
    # Fixture to clean up temporary files and directories created during testing
    yield
    if os.path.exists('temp_file.txt'):
        os.remove('temp_file.txt')
    if os.path.exists('temp_directory'):
        shutil.rmtree('temp_directory')
    
    # Get the absolute path of the current test file
    test_file_path = os.path.abspath(request.node.location[0])

    # Remove nonexistent_directory only if the tests are not from the current file
    if os.path.exists('nonexistent_directory') and test_file_path != os.path.abspath(__file__):
        shutil.rmtree('nonexistent_directory')


def test_transfer_a_file_into_a_dir_successful(cleanup_temp_files):
    # Create a temporary file
    with open('temp_file.txt', 'w') as file:
        file.write('Hello, World!')

    # Perform the file transfer
    assert transfer_a_file_into_a_dir('temp_file.txt', 'temp_directory') is True

    # Check that the renamed file exists in the destination directory
    assert os.path.exists('temp_directory/temp_file_0.txt') is True

    # Check that the original file does not exist in the source directory
    assert os.path.exists('temp_file.txt') is False

def test_transfer_a_file_into_a_dir_destination_directory_not_found(cleanup_temp_files):
    # Create a temporary file
    with open('temp_file.txt', 'w') as file:
        file.write('Hello, World!')

    # Attempt to transfer the file to a nonexistent directory
    assert transfer_a_file_into_a_dir('temp_file.txt', 'nonexistent_directory') is True

    # Check that the file no longer exists in the source directory
    assert os.path.exists('temp_file.txt') is False

    # Check that the destination directory was created
    assert os.path.exists('nonexistent_directory') is True

    # Check that the file exists in the destination directory
    assert os.path.exists('nonexistent_directory/temp_file_0.txt') is True


def test_transfer_a_file_into_a_dir_file_not_found(cleanup_temp_files):
    # Attempt to transfer a nonexistent file
    assert transfer_a_file_into_a_dir('nonexistent_file.txt', 'temp_directory') is False

    # Check that the destination directory was not created
    assert os.path.exists('nonexistent_file.txt') is False


# Clean up after tests
def teardown_module():
    # Remove temporary files and directories created during testing
    if os.path.exists('new_temp_file.txt'):
        os.remove('new_temp_file.txt')
    if os.path.exists('temp_directory'):
        shutil.rmtree('temp_directory')
    if os.path.exists('nonexistent_directory'):
        shutil.rmtree('nonexistent_directory', ignore_errors=True)#ignore errors to remove not empty dir
