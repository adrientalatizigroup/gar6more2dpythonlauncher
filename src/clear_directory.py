"""
Directory Cleaning Script

This script checks if the 'Results' directory is empty. If not, it prompts the user
to confirm whether they want to clean the directory by removing all files.

Functions:
1. is_directory_empty(directory):
   - Checks if a directory is empty.
   - Args:
       - directory (str): The path to the directory.
   - Returns:
       - bool: True if the directory is empty, False otherwise.

2. clean_directory(directory):
   - Cleans a directory by removing all files.
   - Args:
       - directory (str): The path to the directory.

3. ask_for_clear_results():
   - Main function to check and clean the 'Results' directory if needed.

Usage:
   Run the script to interactively check and clean the 'Results' directory.
"""

import os

def is_directory_empty(directory):
    """
    Check if a directory is empty.

    Args:
        directory (str): The path to the directory.

    Returns:
        bool: True if the directory is empty, False otherwise.
    """
    return not any(os.listdir(directory))

def clean_directory(directory):
    """
    Clean a directory by removing all files.

    Args:
        directory (str): The path to the directory.
    """
    for filename in os.listdir(directory):
        file_path = os.path.join(directory, filename)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(f"Error cleaning file {file_path}: {e}")

def ask_for_clear_results():
    """
    Asks the user if they want to clear the 'Results' directory.

    The function checks if the 'Results' directory is empty. If it is not empty, it prompts the user
    to confirm whether they want to clean it. If the user confirms, the directory is cleaned by 
    removing all its contents.

    Returns:
    - None
    """
    results_directory = "gar6more2dpythonlauncher/Results"

    if not is_directory_empty(results_directory):
        user_input = input("The 'Results' directory is not empty. Do you want to clean it? (y/n): ")
        if user_input.lower() == 'y':
            clean_directory(results_directory)
            print("Directory cleaned.")
        else:
            print("Directory not cleaned.")



def ask_create_results_directory():
    """
    Ask the user if they want to create the 'Results' directory if it does not exist.
    """
    results_directory = "gar6more2dpythonlauncher/Results"

    # Check if the directory already exists
    if os.path.exists(results_directory):
        print(f"The '{results_directory}' directory already exists.")
    else:
        # Ask the user if they want to create the directory
        user_input = input(f"The '{results_directory}' directory does not exist. Do you want to create it? (y/n): ")
        if user_input.lower() == 'y':
            try:
                # Create the directory
                os.makedirs(results_directory)
                print(f"The '{results_directory}' directory has been created.")
            except Exception as e:
                print(f"Error creating directory: {e}")
        else:
            print("Directory not created.")