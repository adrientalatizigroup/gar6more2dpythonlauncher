"""
Unit tests for the check_current_directory function.

These tests verify the behavior of the check_current_directory function in your_module.py.
The function is tested under two scenarios:
1. When the current directory matches the expected directory.
2. When the current directory does not match the expected directory.
"""

from unittest.mock import patch
from gar6more2dpythonlauncher.src.check_current_directory import check_current_directory
import pytest
from unittest.mock import patch

def test_current_directory_matches_expected():
    # Patching os.getcwd to return the expected directory
    with patch('os.getcwd', return_value='/path/to/expected_directory') as mock_getcwd:
        # Calling the function under test
        check_current_directory('expected_directory')
        # Verifying that os.getcwd was called once
        mock_getcwd.assert_called_once()

def test_current_directory_not_match_expected():
    # Patching os.getcwd to return a different directory
    with patch('os.getcwd', return_value='/path/to/another_directory'):
        # Checking if the function raises RuntimeError with expected error message
        with pytest.raises(RuntimeError) as context:
            check_current_directory('expected_directory')
        # Verifying the error message content
        assert "Error: Expected to be in directory 'expected_directory', but current directory is '/path/to/another_directory'." in str(context.value)
