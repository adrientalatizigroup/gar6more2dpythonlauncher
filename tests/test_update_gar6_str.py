"""
Unit Tests for update_gar6_str_with_new_reicv_height Function

This file contains pytest unit tests for the update_gar6_str_with_new_reicv_height function
from the gar6more2dpythonlauncher.src.txt_file_gestion_fcts module. The tests cover scenarios for updating the
height of the line of receivers in an existing file, handling non-existent lines, and handling
non-existent files.

Test Descriptions:
- test_update_gar6_str_with_new_reicv_height_existing_line:
  Checks that the function updates the height in an existing file as expected for different input values.

- test_update_gar6_str_with_new_reicv_height_nonexistent_line:
  Checks that the function returns the expected error message when trying to update a non-existent line.

- test_update_gar6_str_with_new_reicv_height_nonexistent_file:
  Checks that the function raises a FileNotFoundError for a non-existent file.

Note: The tests use the pytest.raises context manager to verify the expected exceptions.

Date: 09/11/2023
"""

import pytest

# Assuming your function and get_lines_of_file are defined in a module named 'your_module'
from gar6more2dpythonlauncher.src.txt_file_gestion_fcts import update_gar6_str_with_new_reicv_height, get_lines_of_file


@pytest.mark.parametrize("initial_height, new_height, expected_updated_height", [
    (25, 42, 42.0),
    (10, 56, 56.0),
    (30, 18, 18.0),
    (-0.200001, -1.80025, -1.80025),
])
def test_update_gar6_str_with_new_reicv_height_existing_line(tmp_path, initial_height, new_height, expected_updated_height):
    # Create a temporary file with a known content
    test_file = tmp_path / "test_file.txt"
    test_file.write_text(f"Some content\n{initial_height} Height of the line of receivers\nMore content\n")

    # Update the height using the function
    update_gar6_str_with_new_reicv_height(test_file, new_height=new_height)

    # Read the updated content
    updated_content = get_lines_of_file(test_file)

    # Check if the height is updated
    assert any(f"{expected_updated_height} Height of the line of receivers" in el for el in updated_content)


def test_update_gar6_str_with_new_reicv_height_nonexistent_line(tmp_path):
    # Create a temporary file with a known content
    test_file = tmp_path / "test_file.txt"
    test_file.write_text("Some content\nOther line\nMore content\n")

    # Update the height using the function, expecting an error message
    error_message = update_gar6_str_with_new_reicv_height(test_file, new_height=42)

    # Check if the error message is as expected
    assert "Error: Line 'Height of the line of receivers' not found" in error_message


def test_update_gar6_str_with_new_reicv_height_nonexistent_file():
    # Test that the function raises a FileNotFoundError for a non-existent file
    with pytest.raises(FileNotFoundError):
        update_gar6_str_with_new_reicv_height("nonexistent_file.txt", new_height=42)
