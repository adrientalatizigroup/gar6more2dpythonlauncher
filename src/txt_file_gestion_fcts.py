

"""
File Handling Functions Overview

This module contains three functions related to file handling: check_file_exists, get_lines_of_file,
and update_gar6_str_with_new_reicv_height. Each function serves a specific purpose in checking file
existence, reading lines from a text file, and updating the content of a file, respectively.

Function Descriptions:

1. check_file_exists(filename):
   - Checks if the specified file exists.
   - Raises a FileNotFoundError if the file does not exist.
   - Parameters:
       - filename (str): The name of the file to check.

2. get_lines_of_file(filename: str) -> list[str]:
   - Gets the lines of a text file.
   - Checks file existence using check_file_exists before reading the lines.
   - Raises a FileNotFoundError if the file does not exist.
   - Parameters:
       - filename (str): The name of the file to read.
   - Returns:
       - list[str]: A list containing the lines of the file.

3. update_gar6_str_with_new_reicv_height(filename, new_height):
   - Updates the height of the line of receivers in the specified file.
   - Reads, modifies, and writes the file content based on the new height.
   - Returns an error message if the specified line is not found.
   - Parameters:
       - filename (str): The name of the file to update.
       - new_height (float): The new height of the line of receivers.
   - Returns:
       - str or None: An error message if the line is not found, otherwise None.

Date: 09/11/2023
"""



def check_file_exists(filename):
    """
    @brief Check if the specified file exists.

    This function raises a FileNotFoundError if the specified file does not exist.

    @param filename: The name of the file to check.
    """
    try:
        with open(filename, "r"):
            pass
    except FileNotFoundError:
        raise FileNotFoundError(f"Error: File '{filename}' not found.")


def get_lines_of_file(filename: str):
    """
    Get the lines of a text file.

    This function takes the filename as input, checks if the file exists using the
    check_file_exists function, and then reads and returns the lines of the file.

    Args:
        filename (str): The name of the file to read.

    Returns:
        list[str]: A list containing the lines of the file.

    Raises:
        FileNotFoundError: If the specified file does not exist.
    """
    # Check if the file exists before proceeding
    check_file_exists(filename)

    # Initialize an empty list to store the lines of the file
    lines = list()

    # Read the lines of the file and store them in the 'lines' list
    with open(filename, "r") as f:
        lines = f.readlines()

    # Return the list of lines
    return lines




def update_gar6_str_with_new_reicv_height(filename, new_height):
    """
    Update the height of the line of receivers in the specified file.

    This function reads the content of the specified file, searches for the line containing the height of the line of receivers,
    updates it with the new height, and then writes the modified content back to the file.

    If the specified line is not found in the file, the function returns an error message.

    Parameters:
        filename (str): The name of the file to update.
        new_height (float): The new height of the line of receivers.

    Returns:
        str or None: A string containing the error message if the line is not found, otherwise None.
    """

    # Get the lines of the file
    lines = get_lines_of_file(filename)

    # Find the line containing "Height of the line of receivers"
    line_found = False
    for i, line in enumerate(lines):
        if "Height of the line of receivers" in line:
            lines[i] = f"{float(new_height)} Height of the line of receivers\n"
            line_found = True
            break

    if not line_found:
        return f"Error: Line 'Height of the line of receivers' not found in file '{filename}'."

    # Create a new content string with the updated information
    new_file_content = "".join(lines)

    # Write the updated content back to the file
    with open(filename, "w") as f:
        f.write(new_file_content)

    # Return None if the update is successful
    return None

