"""
Unit Tests for check_file_exists Function

This file contains pytest unit tests for the check_file_exists function from the
gar6more2dpythonlauncher.src.txt_file_gestion_fcts module. The tests cover scenarios for both existing
and non-existent files, ensuring the function behaves correctly in different cases.

Test Descriptions:
- test_check_file_exists_existing_file: 
  Checks that check_file_exists does not raise an error for an existing file.

- test_check_file_exists_nonexistent_file:
  Checks that check_file_exists raises a FileNotFoundError for a non-existent file.

Note: The tests use the pytest.raises context manager to verify the expected exceptions.

Date: 09/11/2023
"""

import pytest

from gar6more2dpythonlauncher.src.txt_file_gestion_fcts import check_file_exists

def test_check_file_exists_existing_file(tmp_path):
    """
    Test the check_file_exists function for an existing file.

    This test creates a temporary file, checks that the check_file_exists function
    does not raise an error when called with the path of an existing file.

    Args:
        tmp_path: pytest fixture providing a temporary directory path
    """
    # Create a temporary file
    test_file = tmp_path / "test_file.txt"
    test_file.touch()

    # Test that the function does not raise an error for an existing file
    check_file_exists(test_file)

def test_check_file_exists_nonexistent_file():
    """
    Test the check_file_exists function for a non-existent file.

    This test checks that the check_file_exists function raises a FileNotFoundError
    when called with the path of a non-existent file.

    Note: The test uses the pytest.raises context manager to check for the expected exception.
    """
    # Test that the function raises a FileNotFoundError for a non-existent file
    with pytest.raises(FileNotFoundError):
        check_file_exists("nonexistent_file.txt")
