import meshio
import numpy as np
import argparse


class GridNode:
    def __init__(self, x, y):
        """
        Initializes a GridNode object with x and y coordinates.

        Parameters:
        - x (float): The x-coordinate of the node.
        - y (float): The y-coordinate of the node.
        """
        self.x = x  # Initialize the x-coordinate of the node
        self.y = y  # Initialize the y-coordinate of the node

class Quadrangle:
    def __init__(self, N1, N2, N3, N4):
        """
        Initializes a Quadrangle object with four node tags.

        Parameters:
        - N1 (int): The tag of the first node.
        - N2 (int): The tag of the second node.
        - N3 (int): The tag of the third node.
        - N4 (int): The tag of the fourth node.

        Attributes:
        - N1 (int): The tag of the first node.
        - N2 (int): The tag of the second node.
        - N3 (int): The tag of the third node.
        - N4 (int): The tag of the fourth node.
        - node_tags (list): A list containing the tags of all four nodes.
        """
        self.N1 = N1
        self.N2 = N2
        self.N3 = N3
        self.N4 = N4
        self.node_tags = [N1, N2, N3, N4]


class Triangle:
    def __init__(self, N1, N2, N3):
        """
        Initializes a Triangle object with three node tags.

        Parameters:
        - N1 (int): The tag of the first node.
        - N2 (int): The tag of the second node.
        - N3 (int): The tag of the third node.

        Attributes:
        - N1 (int): The tag of the first node.
        - N2 (int): The tag of the second node.
        - N3 (int): The tag of the third node.
        - node_tags (list): A list containing the tags of all three nodes.
        """
        self.N1 = N1
        self.N2 = N2
        self.N3 = N3
        self.node_tags = [N1, N2, N3]



class StructuredGridQuad:
    """
    Initializes a StructuredGrid object.

    Parameters:
    - length (float): Length of one side of the grid.
    - n_squares (int): Number of segments in each direction.
    """
    def __init__(self, length, n_squares):
        # Initialize lists to store nodes and quadrangles
        self.nodes = list()
        self.quadrangles = list()
        self.fields= None

        # Store grid parameters
        self.length = length
        self.n_squares = n_squares
        self.nb_of_nodes_per_line = n_squares + 1
        
        # Initialize variables for grid construction
        self.length_vec = None
        self.setup_grid = list()

        # Build the grid
        self.__build_grid()

        points = [[el.x, el.y] for el in self.nodes]
        # Define cells as quadrangles based on node tags from the Mesh quadrangles
        cells = [("quad", [el.node_tags for el in self.quadrangles])]
        
        self.MeshioMesh= meshio.Mesh(
            points,
            cells,
        )# Extract node coordinates from the Mesh
    
    
    def add_field(self, FieldName, ListOfNodeValues):
        """
        Adds a field to the mesh.

        Parameters:
        - FieldName (str): The name of the field.
        - ListOfNodeValues (list): A list containing the values of the field for each node.

        Returns:
        - None
        """
        self.MeshioMesh.point_data[FieldName] = np.array(ListOfNodeValues)

    
    def dump(self, FileName):
        """
        Writes the mesh to a file.

        Parameters:
        - FileName (str): The name of the file to write the mesh to.

        Returns:
        - None
        """
        self.MeshioMesh.write(f"{FileName}.vtu", file_format="vtu", binary=False)

    def __build_setup_grid(self):
        """
        Builds the setup grid.

        This grid is used to define the initial node positions of the structured grid.

        """
        # Calculate start and end lengths for the grid
        start_length = -self.length / 2
        end_length = self.length / 2
        
        # reset setup grid
        self.setup_grid= list()

        # Create evenly spaced vector of lengths
        self.length_vec = np.linspace(start_length, end_length, self.nb_of_nodes_per_line)

        # Iterate over the length vector to create grid lines
        for y in self.length_vec:
            line = list()  # Initialize list to store nodes for the current line
            for x in self.length_vec:
                # Round node coordinates to 4 decimal places and append to line
                line.append((round(x, 6), round(y, 6)))
            # Append the line to the setup grid
            self.setup_grid.append(line)
    
    def __build_quadrangle_tags(self, P0_idx, P1_idx, P2_idx, P3_idx):
        """
        Builds quadrangle tags based on node indexes.

        Parameters:
        - P0_idx (int): Index of the first node.
        - P1_idx (int): Index of the second node.
        - P2_idx (int): Index of the third node.
        - P3_idx (int): Index of the fourth node.
        - P4_idx (int): Index of the fifth node.

        Returns:
        - T1, T2, T3, T4 (quadrangle): quadrangle objects.
        """
        # Create quadrangles with the specified node indexes
        Q = Quadrangle(P0_idx, P1_idx, P2_idx, P3_idx)
        # Return the created quadrangles
        return Q

    def __build_node_indexes_from_grid_position(self, i, j):
        """
        Calculates node indexes from grid position.

        Parameters:
        - i (int): Row index.
        - j (int): Column index.

        Returns:
        - P0_idx, P1_idx, P2_idx, P3_idx(int): Node indexes.
        """
        # Calculate node indexes based on grid position
        P0_idx = i * self.nb_of_nodes_per_line + j
        P1_idx = i * self.nb_of_nodes_per_line + j + 1
        P2_idx = (i + 1) * self.nb_of_nodes_per_line + j + 1
        P3_idx = (i + 1) * self.nb_of_nodes_per_line + j
        # Return the calculated node indexes
        return P0_idx, P1_idx, P2_idx, P3_idx

    def __build_nodes(self):
        """
        Builds nodes based on the setup grid and center nodes.

        This method populates the nodes list with GridNode objects based on the coordinates
        provided in the setup grid and center nodes lists.
        """
        # reset nodes
        self.nodes= list()
        # Add nodes from the setup grid
        for line in self.setup_grid:
            for el in line:
                # Create a GridNode object with coordinates from the setup grid and append it to the nodes list
                self.nodes.append(GridNode(el[0], el[1]))
    
    def __build_grid(self):
        """
        Builds the structured grid.

        This method orchestrates the construction of the structured grid, including setting up the grid, calculating
        center nodes, defining quadrangles, and creating nodes.
        """
        # Build the setup grid
        self.__build_setup_grid()

        # Iterate over each square in the grid
        for i in range(self.n_squares):
            for j in range(self.n_squares):

                # Calculate node indexes for the current square
                P0_idx, P1_idx, P2_idx, P3_idx = self.__build_node_indexes_from_grid_position(i, j)

                # Build quadrangles for the current square and add them to the list
                Q = self.__build_quadrangle_tags(P0_idx, P1_idx, P2_idx, P3_idx)
                self.quadrangles.extend([Q])

        # Build nodes based on the setup grid and center nodes
        self.__build_nodes()


class StructuredGridTri:
    """
    Initializes a StructuredGrid object.

    Parameters:
    - length (float): Length of one side of the grid.
    - n_squares (int): Number of segments in each direction.
    """
    def __init__(self, length, n_squares):
        # Initialize lists to store nodes and quadrangles
        self.nodes = list()
        self.triangles = list()
        self.fields= None

        # Store grid parameters
        self.length = length
        self.n_squares = n_squares
        self.nb_of_nodes_per_line = n_squares + 1
        
        # Initialize variables for grid construction
        self.length_vec = None
        self.setup_grid = list()

        # Build the grid
        self.__build_grid()

        points = [[el.x, el.y] for el in self.nodes]
        # Define cells as quadrangles based on node tags from the Mesh quadrangles
        cells = [("triangle", [el.node_tags for el in self.triangles])]
        
        self.MeshioMesh= meshio.Mesh(
            points,
            cells,
        )# Extract node coordinates from the Mesh
    
    
    def add_field(self, FieldName, ListOfNodeValues):
        """
        Adds a field to the mesh.

        Parameters:
        - FieldName (str): The name of the field.
        - ListOfNodeValues (list): A list containing the values of the field for each node.

        Returns:
        - None
        """
        self.MeshioMesh.point_data[FieldName] = np.array(ListOfNodeValues)

    def dump(self, FileName):
        """
        Writes the mesh to a file.

        Parameters:
        - FileName (str): The name of the file to write the mesh to.

        Returns:
        - None
        """
        self.MeshioMesh.write(f"{FileName}.vtu", file_format="vtu", binary=True) #binary=False

    def __build_setup_grid(self):
        """
        Builds the setup grid.

        This grid is used to define the initial node positions of the structured grid.

        """
        # Calculate start and end lengths for the grid
        start_length = -self.length / 2
        end_length = self.length / 2

        # Create evenly spaced vector of lengths
        self.length_vec = np.linspace(start_length, end_length, self.nb_of_nodes_per_line)

        # Iterate over the length vector to create grid lines
        for y in self.length_vec:
            line = list()  # Initialize list to store nodes for the current line
            for x in self.length_vec:
                # Round node coordinates to 4 decimal places and append to line
                line.append((round(x, 6), round(y, 6)))
            # Append the line to the setup grid
            self.setup_grid.append(line)
    
    def __build_triangle_tags(self, P0_idx, P1_idx, P2_idx, P3_idx):
        """
        Builds triangles tags based on node indexes.

        Parameters:
        - P0_idx (int): Index of the first node.
        - P1_idx (int): Index of the second node.
        - P2_idx (int): Index of the third node.
        - P3_idx (int): Index of the fourth node.

        Returns:
        - T1, T2 (triangles): Triangle objects.
        """
        # Create triangles with the specified node indexes
        T1= Triangle(P0_idx, P1_idx, P2_idx)
        T2= Triangle(P0_idx, P2_idx, P3_idx)
        # Return the created triangles
        return T1, T2

    def __build_node_indexes_from_grid_position(self, i, j):
        """
        Calculates node indexes from grid position.

        Parameters:
        - i (int): Row index.
        - j (int): Column index.

        Returns:
        - P0_idx, P1_idx, P2_idx, P3_idx(int): Node indexes.
        """
        # Calculate node indexes based on grid position
        P0_idx = i * self.nb_of_nodes_per_line + j
        P1_idx = i * self.nb_of_nodes_per_line + j + 1
        P2_idx = (i + 1) * self.nb_of_nodes_per_line + j + 1
        P3_idx = (i + 1) * self.nb_of_nodes_per_line + j
        # Return the calculated node indexes
        return P0_idx, P1_idx, P2_idx, P3_idx

    def __build_nodes(self):
        """
        Builds nodes based on the setup grid and center nodes.

        This method populates the nodes list with GridNode objects based on the coordinates
        provided in the setup grid and center nodes lists.
        """
        # Add nodes from the setup grid
        for line in self.setup_grid:
            for el in line:
                # Create a GridNode object with coordinates from the setup grid and append it to the nodes list
                self.nodes.append(GridNode(el[0], el[1]))
    
    def __build_grid(self):
        """
        Builds the structured grid.

        This method orchestrates the construction of the structured grid, including setting up the grid, calculating
        center nodes, defining triangles, and creating nodes.
        """
        # Build the setup grid
        self.__build_setup_grid()

        # Iterate over each square in the grid
        for i in range(self.n_squares):
            for j in range(self.n_squares):

                # Calculate node indexes for the current square
                P0_idx, P1_idx, P2_idx, P3_idx = self.__build_node_indexes_from_grid_position(i, j)

                # Build triangles for the current square and add them to the list
                T1, T2 = self.__build_triangle_tags(P0_idx, P1_idx, P2_idx, P3_idx)
                self.triangles.extend([T1, T2])

        # Build nodes based on the setup grid and center nodes
        self.__build_nodes()





if __name__ == "__main__":
    # Create argument parser to handle command-line arguments
    parser = argparse.ArgumentParser(description="Generate structured mesh")
    
    # Define command-line arguments
    parser.add_argument("size", type=int, help="Size of a side of the square")
    parser.add_argument("n_squares", type=int, help="Number of segments (= [nodes-1]) in x/y direction")
    
    # Parse command-line arguments
    args = parser.parse_args()

    # Create a StructuredGrid object based on the provided size and number of squares
    Mesh = StructuredGridTri(args.size, args.n_squares)
    Mesh.add_field("coucou", [(i,i) for i in range(int((args.n_squares+1)*(args.n_squares+1)))])
    Mesh.dump("Res")
    