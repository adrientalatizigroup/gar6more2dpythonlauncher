import subprocess
import numpy as np
import argparse
from src.clear_directory import ask_for_clear_results, ask_create_results_directory
from src.txt_file_gestion_fcts import update_gar6_str_with_new_reicv_height
from src.gather_results import transfer_result_files_into_result_directory
from src.post_treat_gar6_res import get_Ux_and_Uy_to_dict
from src.generate_vtu_files import StructuredGridQuad, StructuredGridTri

import numpy as np
import subprocess

def gar6_multiple_launches(start_height, end_heights, n_height_steps):
    """
    Launch multiple runs of the Gar6more2D simulation with varying receiver heights.

    Parameters:
    - start_height (float): The starting height for the receiver.
    - end_heights (float): The ending height for the receiver.
    - n_height_steps (int): The number of steps or runs to perform.

    Returns:
    - None
    """
    # Generate an array of heights with n_height_steps+1 points from start_height to end_heights
    heights = np.linspace(start_height, end_heights, int(n_height_steps + 1))

    # Loop through each height and launch the simulation
    for height in heights:
        print(f"Current Height running: {height}")
        # Update the input file with the new receiver height
        update_gar6_str_with_new_reicv_height("Gar6more2D.dat", height)
        # Launch the Gar6more2D simulation
        launch_gar6()
        # Transfer result files into the result directory
        transfer_result_files_into_result_directory()

def launch_gar6():
    """
    Launch the Gar6more2D simulation.

    Returns:
    - subprocess.Popen or None: The subprocess representing the launched process or None if there is an error.
    """
    # Command to execute the Gar6more2D simulation
    command = "./build/Gar6more2D.out"
    try:
        # Launch the process
        process = subprocess.Popen(command, shell=True)
        # Wait for the process to complete
        process.wait()
        return process
    except Exception as e:
        # Handle any exceptions during the launch
        print(f"Error launching Gar6more2D: {e}")
        return None

def output_vtu(n_height_steps):
    d_grid= get_Ux_and_Uy_to_dict(n_height_steps)
    frames= list(d_grid.keys())
    frames.sort()
    field=[]
    for frame in frames:
        for line in d_grid[frame]:
            field.extend(line)
        Grid= StructuredGridTri(2, int(n_height_steps-1))
        field.reverse()
        Grid.add_field("U", field)
        Grid.dump(f"Result_{frame}")
        field= list()


def main():
    # launch command: python3 gar6more2dpythonlauncher/launcher.py --range -1.0 1.0 10

    # Default values for height range and number of steps
    start_height = -1.0
    end_height = 1.0
    n_height_steps = 10

    # Create an ArgumentParser
    parser = argparse.ArgumentParser(description='gar6more2d grid creation')

    # Add the --range option with nargs=3 to accept three values
    parser.add_argument('--range', nargs=3, type=float, default=[-1.0, 1.0, 10],
                        help='Specify the range as X Y n (default: -1.0 1.0 10)')

    # Parse the command-line arguments
    args = parser.parse_args()

    # Extract X, Y, and n from the range option
    start_height, end_height, n_height_steps = args.range

    # Display information about the selected height range and steps
    start_message = f"*********************************\n"
    start_message += f"*** GAR6MORE PY LAUNCHER INFO ***\n"
    start_message += f"*********************************\n\n"
    start_message += f"=> Selected Starting Height is: {start_height}.\n"
    start_message += f"=> Selected Ending Height is: {end_height}.\n"
    start_message += f"=> Selected Height discretization is: {n_height_steps} steps.\n"

    print(start_message)

    # Ask the user to create the results directory
    ask_create_results_directory()

    # Ask the user whether to clear existing results
    ask_for_clear_results()

    # Launch multiple runs of the Gar6more2D simulation
    gar6_multiple_launches(start_height, end_height, n_height_steps)

    # Gather results files into .vtu files
    output_vtu(n_height_steps)

if __name__ == '__main__':
    main()
